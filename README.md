This is an e-commerce application built for a friend that will exercise my ability to code in JavaScript frameworks. Server-side code is written in Node.js (Express.js), the current front-end code is written using the EJS templating engine with server-side rendering of data.

The goal of the application is to stress my implementation skills by progressively building the application around interaction with the file system, relational databases (MySQL), and NoSQL databases (MongoDB). With the final product utilizing MongoDB. It is also my goal to swap from using templating engines to use this project as an opportunity to learn Angular for front-end.

Thanks for stopping by!