const Product = require('../models/product');


function getAddProduct (req, res, next) {
    res.render('admin/edit-product', {
        pageTitle: 'Admin: Add Product',
        path: '/admin/add-product',
        editing: false
    });
};

function postAddProduct (req, res, next) {
    const title = req.body.title;
    const imageURL = req.body.imageURL;
    const price = req.body.price;
    const description = req.body.description;
    req.user.createProduct({
        title: title,
        imageURL: imageURL,
        price: price,
        description: description
    })
    .then(result => {
        console.log('Product added.');
        res.redirect('/admin/manage-product');
    })
    .catch(err => {
        console.log(err);
    });
};

function postDeleteProduct (req, res, next) {
    const prodId = req.body.productId;
    Product.findByPk(prodId)
    .then((product) => {
        return product.destroy();
    })
    .then((result) => {
        console.log(`Product with ID ${prodId} deleted from the database.`);
        res.redirect('/admin/manage-product');
    })
    .catch((err) => {
        console.log(err);
    });
}

function getEditProduct (req, res, next) {
    const editMode = req.query.editMode;
    if (!editMode) {
        res.redirect('/')
    }
    else {
        const prodId = req.params.productId;
        req.user.getProducts({where: {id: prodId}}) // Gets products associated with current user
        // Product.findByPk(prodId) // Gets all products regardless of user
        .then(products => {
            const product = products[0];
            res.render('admin/edit-product', {
                pageTitle: 'Admin: Edit Product',
                path: '/admin/edit-product',
                editing: editMode,
                product: product
            });
        })
        .catch((err) => {
            console.log(err);
        }); 
    }
};

function postEditProduct (req, res, next) {
    const prodId = req.body.productId;
    const updatedTitle = req.body.title;
    const updatedPrice = req.body.price;
    const updatedImageLink = req.body.imageLink;
    const updatedDescription = req.body.description;
    Product.findByPk(prodId)
    .then((product) => {
        product.title = updatedTitle;
        product.price = updatedPrice;
        product.imageLink = updatedImageLink;
        product.description = updatedDescription;
        return product.save();
    })
    .then((result) => {
        console.log(`Updated product ID: ${prodId}`);
        res.redirect('/admin/manage-product');
    })
    .catch((err) => {
        console.log(err);
    });
}

function getManageProduct (req, res, next) {
    Product.findAll()
    .then(products => {
        res.render('admin/manage-product', {
            prods: products,
            pageTitle: 'Admin: Product Manager',
            path: '/admin/manage-product',
        });
    })
    .catch(err => {
        console.log(err);
    });
};


module.exports = {
    getAddProduct,
    postAddProduct,
    postDeleteProduct,
    getEditProduct,
    postEditProduct,
    getManageProduct,
}