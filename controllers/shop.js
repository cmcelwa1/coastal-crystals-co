const Product = require('../models/product');
const Cart = require('../models/cart');

function getCart (req, res, next) {
    req.user.getCart()
    .then(cart => {
        return cart.getProducts()
        .then(cartProducts => {
            res.render('shop/cart', {
                path: '/cart',
                pageTitle: 'Shopping Cart',
                products: cartProducts
            });
        })
        .catch(err => console.log(err));
    })
    .catch(err => console.log(err));
};

function postCart(req, res, next) {
    const prodId = req.body.productId;
    let newQuantity = 1;
    let fetchedCart;
    req.user.getCart()
    .then(cart => {
        fetchedCart = cart;
        return cart.getProducts({where: { id: prodId }});
    })
    .then(products => {
        let product;
        if (products.length > 0) {
            product = products[0];
        }
        // If product is already found in cart, else leave quantity as 1
        if (product) {
            const oldQuantity = product.cartItem.quantity;
            newQuantity = 1 + oldQuantity;
            return product;
        }
        return Product.findByPk(prodId);
    })
    .then(product => {
        return fetchedCart.addProduct(product, {
            through: { quantity: newQuantity }
        });
    })
    .then(() => {
        res.redirect('/cart');
    })
    .catch(err => console.log(err));
}

function postCartDeleteProduct(req, res, next) {
    const prodId = req.body.productId;
    req.user.getCart()
    .then(cart => {
        return cart.getProducts({ where: { id: prodId }});
    })
    .then(cartProducts => {
        const product = cartProducts[0];
        return product.cartItem.destroy();
    })
    .then(() => {
        res.redirect('/cart');
    })
    .catch(err => console.log(err));
}

// TODO
function getCheckout (req, res, next) {
    res.render('shop/checkout', {
        pageTitle: 'Checkout',
        path: '/checkout',
    });
};

function getIndex (req, res, next) {
    Product.findAll()
    .then(products => {
        res.render('shop/index', {
            prods: products,
            pageTitle: 'Shop Index',
            path: '/'
        });
    })
    .catch(err => {
        console.log(err);
    });
};

// TODO
function getOrders (req, res, next) {
    res.render('shop/orders', {
        pageTitle: 'Orders',
        path: '/orders',
    });
}
function getProductDetail (req, res, next) {
    const prodId = req.params.productId;
    Product.findByPk(prodId)
    .then(product => {
        res.render('shop/product-detail', {
            product: product,
            pageTitle: `${product.title}: Product Detail`,
            path: '/products',
        });
    })
    .catch(err => {
        console.log(err);
    })
}

function getProducts (req, res, next) {
    Product.findAll()
    .then(products => {
        res.render('shop/product-list', {
            prods: products,
            pageTitle: 'All Products',
            path: '/products'
        })
    })
    .catch(err => {
        console.log(err);
    });
};


module.exports = {
    getCart,
    postCart,
    postCartDeleteProduct,
    getCheckout,
    getIndex,
    getOrders,
    getProductDetail,
    getProducts,
}