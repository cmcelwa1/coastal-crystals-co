require('dotenv').config();
const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');

const errController = require('./controllers/error');
const sequelize = require('./util/database');
const Product = require('./models/product');
const User = require('./models/user');
const Cart = require('./models/cart');
const CartItem = require('./models/cartitem');

const app = express();

app.set('view engine', 'ejs');
app.set('views', 'views');

const adminRouter = require('./routes/admin');
const shopRouter = require('./routes/shop');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use((req, res, next) => {
    User.findByPk(1)
    .then(user => {
        req.user = user; // Sequelize version so that we can use sequelize model functions.
        next();
    })
    .catch(err => {
        console.log(err);
    });
});

app.use('/admin', adminRouter.routes);
app.use(shopRouter.routes);

app.use(errController.get404);

const port = process.env.port || 8080;

Product.belongsTo(User, {
    constraints: true,
    onDelete: 'CASCADE'
});
User.hasMany(Product);

User.hasOne(Cart);
Cart.belongsTo(User);

Cart.belongsToMany(Product, { through: CartItem }); // through attribute defines the junction table
Product.belongsToMany(Cart, { through: CartItem });

// Synchronizes models to create tables in the database. Also captures relations.
sequelize
.sync() // set { force: true } to drop and create tables again / refresh schema
.then(result => {
    return User.findByPk(1);
})
.then(userFound => {
    if (!userFound) {
        return User.create({
            username: 'TestUser',
            email: 'test@test.com'
        });
    }
    return userFound;
})
.then(user => {
    // console.log(user.toJSON());
    return user.createCart();
})
.then(cart => {
    app.listen(port);
    console.log(`Server is listening on port ${port}.`);
})
.catch(err => {
    console.log(err);
});

