const {Sequelize} = require('sequelize');

const pw = process.env.dbPW;

const sequelize = new Sequelize(process.env.dbName, process.env.dbUsername, process.env.dbPW, {
    dialect: process.env.dbDialect, 
    host: process.env.dbHost
});

module.exports = sequelize;